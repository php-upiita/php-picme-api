<?php
// Required headers
header("Access-Control-Allow-Origin: http://localhost/picme-api/");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
ini_set("display_errors", E_ALL);


// Required to decode jwt
include_once '../../config/core.php';
include_once '../../libs/php-jwt/src/BeforeValidException.php';
include_once '../../libs/php-jwt/src/ExpiredException.php';
include_once '../../libs/php-jwt/src/SignatureInvalidException.php';
include_once '../../libs/php-jwt/src/JWT.php';
use \Firebase\JWT\JWT;

// get posted data
$data = json_decode(file_get_contents("php://input"));
// get jwt if exits, if not send empty string
$jwt=isset($data->jwt) ? $data->jwt : "";



if($jwt) {
  try {
    // decode jwt
    $decoded = JWT::decode($jwt, $key, array('HS256'));
    http_response_code(200);
    echo json_encode(array("message" => "Token válido.", "data" => $decoded->data, "status" => "ok", "code" => 200));

  } catch (Exception $e) {
    http_response_code(401);
    echo json_encode(array("message" => "Token inválido", "error" => $e->getMessage(), "status" => "error", "code" => 401));
  }
} else {
  // set response code
  http_response_code(400);
  // display message: unable to create user
  echo json_encode(array("message" => "Parámetros enviados incorrectamente, favor de revisar la documentación.", "status" => "error", "code"=>400));
}

?>