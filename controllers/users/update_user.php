<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// Required to decode jwt
include_once '../../config/core.php';
include_once '../../libs/php-jwt/src/BeforeValidException.php';
include_once '../../libs/php-jwt/src/ExpiredException.php';
include_once '../../libs/php-jwt/src/SignatureInvalidException.php';
include_once '../../libs/php-jwt/src/JWT.php';
use \Firebase\JWT\JWT;


// files needed to connect to database
include_once '../../config/database.php';
include_once '../../objects/user.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();

// instantiate product object
$user = new User($db);


// get posted data
$data = json_decode(file_get_contents("php://input"));
 
// get jwt
$jwt=isset($data->jwt) ? $data->jwt : "";
 
// if jwt is not empty
if($jwt){
  // if decode succeed, show user details
  try {

      // decode jwt
      $decoded = JWT::decode($jwt, $key, array('HS256'));

      $user->id = $decoded->data->id;
      $user->email = $data->email;
      $user->password = $data->password;
      $user->firstname = $data->firstname;
      $user->lastname = $data->lastname;
      $user->phone = $data->phone;
      $user->fb = $data->fb;
      $user->ig = $data->ig;


      // update the user record
      if($user->update()){
        // we need to re-generate jwt because user details might be different
        $token = array(
          "iss" => $iss,
          "aud" => $aud,
          "iat" => $iat,
          "nbf" => $nbf,
          "data" => array(
            "id" => $user->id,
            "userType" => $user->userType,
            "email" => $user->email,
            "firstname" => $user->firstname,
            "lastname" => $user->lastname,
            "created_at" => $user->created_at,
            "updated_at" => $user->updated_at,
          )
        );
        $jwt = JWT::encode($token, $key);

        // set response code
        http_response_code(200);

        echo json_encode(array("message" => "Usuario actualizado con éxito.", "jwt" => $jwt,"status" => "ok", "code"=>200));
     
      } else{
        // set response code
        http_response_code(401);

        // show error message
        echo json_encode(array("message" => "No nos fue posible actualizar tu usuario", "status" => "error", "code"=>401));
      }
    
    } catch (Exception $e){
 
  // set response code
  http_response_code(401);

  // show error message
  echo json_encode(array(
      "message" => "Access denied.",
      "error" => $e->getMessage()
  ));
}
}
