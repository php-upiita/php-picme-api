<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// Required to decode jwt
include_once '../../config/core.php';
include_once '../../libs/php-jwt/src/BeforeValidException.php';
include_once '../../libs/php-jwt/src/ExpiredException.php';
include_once '../../libs/php-jwt/src/SignatureInvalidException.php';
include_once '../../libs/php-jwt/src/JWT.php';
use \Firebase\JWT\JWT;


// files needed to connect to database
include_once '../../config/database.php';
include_once '../../objects/match.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();

// instantiate product object
$user = new Match($db);


// get posted data
$data = json_decode(file_get_contents("php://input"));
 
// get jwt
$jwt=isset($data->jwt) ? $data->jwt : "";
 
// if jwt is not empty
if($jwt){
  // if decode succeed, show user details
  try {
    // decode jwt
    $decoded = JWT::decode($jwt, $key, array('HS256'));
    $user->userType = $decoded->data->userType;
    $user->id = $decoded->data->id;


    // query products
    $stmt = $user->getRequest();
    $num = $stmt->rowCount();
    // check if more than 0 record found
    if($num>0){
      // products array
      $models_arr=array();
      $models_arr["matches"]=array();
      while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){      
        extract($row);


        
        $models=array(
                        "id" => $id,
                        "nombre" => $nombre,
                        "apellidos" => $apellidos,
                        "foto_perfil" => $foto_perfil,
                    );
            
            array_push($models_arr["matches"], $models);
      }
      http_response_code(200);
      echo json_encode(array( "data" => $models_arr,"status" => "ok", "code"=>200));
     
      } else{
        // set response code
        http_response_code(401);

        // show error message
        echo json_encode(array("message" => "No hay modelos", "status" => "error", "code"=>404));
      }
    
    } catch (Exception $e){
 
  // set response code
  http_response_code(401);

  // show error message
  echo json_encode(array(
      "message" => "Access denied.",
      "error" => $e->getMessage()
  ));
}
}
