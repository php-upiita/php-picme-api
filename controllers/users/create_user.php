<?php
// required headers
header("Access-Control-Allow-Origin: http://localhost/picme-api/");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
ini_set("display_errors", E_ALL);

   
// files needed to connect to database
include_once '../../config/database.php';
include_once '../../objects/user.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();

// instantiate product object
$user = new User($db);

// get posted data
$data = json_decode(file_get_contents("php://input"));

if( !empty($data->firstname) && !empty($data->email) &&
    !empty($data->password) && !empty($data->userType) &&
    !empty($data->phone)) {

  $user->userType = $data->userType;
  $user->email = $data->email;
  $user->password = $data->password;
  $user->firstname = $data->firstname;
  $user->lastname = $data->lastname;
  $user->phone = $data->phone;
  if($user->create()){
    http_response_code(200);
    // display message: user was created
    echo json_encode(array("message" => "El fue creado exitosamente.", "status" => "ok", "code"=>200));
  }else{
    // set response code
    http_response_code(400);
    // display message: unable to create user
    echo json_encode(array("message" => "El usuario no pudo ser creado.", "status" => "error", "code"=>400));
  }
}else{
  // set response code
  http_response_code(400);
  // display message: unable to create user
  echo json_encode(array("message" => "Parámetros enviados incorrectamente, favor de revisar la documentación.", "status" => "error", "code"=>400));
}
?>