<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// Required to decode jwt
include_once '../../config/core.php';
include_once '../../libs/php-jwt/src/BeforeValidException.php';
include_once '../../libs/php-jwt/src/ExpiredException.php';
include_once '../../libs/php-jwt/src/SignatureInvalidException.php';
include_once '../../libs/php-jwt/src/JWT.php';
use \Firebase\JWT\JWT;


// files needed to connect to database
include_once '../../config/database.php';
include_once '../../objects/user.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();

// instantiate product object
$user = new User($db);


// get posted data
$data = json_decode(file_get_contents("php://input"));
 
// get jwt
$jwt=isset($data->jwt) ? $data->jwt : "";
 
// if jwt is not empty
if($jwt){
  // if decode succeed, show user details
  try {

      // decode jwt
      $decoded = JWT::decode($jwt, $key, array('HS256'));

      $user->userId = $data->id;

      
      // update the user record
      if($user->getModel()){
        $data = array(

            "id" => $user->id,
            "email" => $user->email,
            "firstname" => $user->firstname,
            "lastname" => $user->lastname,
            "phone" => $user->phone,
            "foto_perfil" => $user->profilePic,
            "estatura" => $user->height,
            "color" => $user->hair,
            "tez" => $user->skin,
            "complexion" => $user->body,
            "ojos" => $user->eyes
          
        );

        
        
        // set response code
        http_response_code(200);

        echo json_encode(array( "data" => $data,"status" => "ok", "code"=>200));
     
      } else{
        // set response code
        http_response_code(401);

        // show error message
        echo json_encode(array("message" => "No nos fue posible actualizar tus datos", "status" => "error", "code"=>401));
      }
    
    } catch (Exception $e){
 
  // set response code
  http_response_code(401);

  // show error message
  echo json_encode(array(
      "message" => "Access denied.",
      "error" => $e->getMessage()
  ));
}
}
