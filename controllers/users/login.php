<?php
// required headers
header("Access-Control-Allow-Origin: http://localhost/picme-api/");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
ini_set("display_errors", E_ALL);

   
// files needed to connect to database
include_once '../../config/database.php';
include_once '../../objects/user.php';

// generate json web token
include_once '../../config/core.php';
include_once '../../libs/php-jwt/src/BeforeValidException.php';
include_once '../../libs/php-jwt/src/ExpiredException.php';
include_once '../../libs/php-jwt/src/SignatureInvalidException.php';
include_once '../../libs/php-jwt/src/JWT.php';
use \Firebase\JWT\JWT;
 
// get database connection
$database = new Database();
$db = $database->getConnection();

// instantiate product object
$user = new User($db);

// get posted data
$data = json_decode(file_get_contents("php://input"));

if( !empty($data->email) && !empty($data->password) ) {
  $user->email = $data->email;
  $email_exist = $user->emailExist();
  
  //Validate email and password
  if($email_exist  && password_verify($data->password, $user->password)){
    $token = array(
      "iss" => $iss,
      "aud" => $aud,
      "iat" => $iat,
      "nbf" => $nbf,
      "data" => array(
        "id" => $user->id,
        "userType" => $user->userType,
        "email" => $user->email,
        "firstname" => $user->firstname,
        "lastname" => $user->lastname,
        "created_at" => $user->created_at,
        "updated_at" => $user->updated_at,
      )
    );

    //Set responde code
    http_response_code(200);
    //Generate JWT
    $jwt = JWT::encode($token, $key);
    echo json_encode(array("message" => "Inicio de sesión exitoso.", "jwt" => $jwt,"status" => "ok", "code"=>200));

  }else{
    // set response code
    http_response_code(401);
    // display message: unable to create user
    echo json_encode(array("message" => "Correo electrónico y/o contraseña no encontrados.", "status" => "error", "code"=>401));
  }  

}else{
  // set response code
  http_response_code(400);
  // display message: unable to create user
  echo json_encode(array("message" => "Parámetros enviados incorrectamente, favor de revisar la documentación.", "status" => "error", "code"=>400));
}

?>