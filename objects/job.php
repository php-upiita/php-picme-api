<?php
// 'user' object
class Job {
  // database connection and table name
  private $conn;
  private $table_name = "trabajo";
  // object properties
  public $userId;
  public $name;
  public $description;


  // constructor
  public function __construct($db) {
    $this->conn = $db;
  }
   // create new user record    
   function update() {
    
   try {
     // insert query
     $query = "INSERT INTO " . $this->table_name . "
                (
                  id_usuario,
                  nombre,
                  descripcion
                )
                VALUES(
                  :userId,
                  :description,
                  :name
                )";

     // prepare the query
   
     $stmt = $this->conn->prepare($query);
     // bind the values
     $stmt->bindValue(':userId', trim($this->userId), PDO::PARAM_INT);
     $stmt->bindValue(':description', trim($this->description), PDO::PARAM_STR);
     $stmt->bindValue(':name', trim($this->name), PDO::PARAM_STR);

       
     // execute the query, also check if query was successful
     if($stmt->execute()){
     
      
       return true;
     }
     return false;
   }catch(Exception $ex){
     // set response code
     http_response_code(400);
     // display message: unable to create user
     echo json_encode(array("message" => $ex->getMessage(), "status" => "error", "code"=>$ex->getCode()));
   }
 }

  

}



