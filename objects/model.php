<?php
// 'user' object
class Model {
  // database connection and table name
  private $conn;
  private $table_name = "modelo";
  // object properties
  public $userId;
  public $heigth;
  public $hair;
  public $skin;
  public $body;
  public $eyes;

  // constructor
  public function __construct($db) {
    $this->conn = $db;
  }
   // create new user record    
   function update() {
    
    try {
      // insert query
      $query = "UPDATE " . $this->table_name . "
                SET
                  estatura = :height,
                  id_cabello = :hair,
                  id_tez = :skin,
                  id_complexion = :body,
                  id_ojos = :eyes
                WHERE id_usuario = :userId;";

      // prepare the query
    
      $stmt = $this->conn->prepare($query);
      // bind the values
      $stmt->bindValue(':userId', trim($this->userId), PDO::PARAM_INT);
      $stmt->bindValue(':height', trim($this->heigth), PDO::PARAM_INT);
      $stmt->bindValue(':hair', trim($this->hair), PDO::PARAM_INT);
      $stmt->bindValue(':skin', trim($this->skin), PDO::PARAM_INT);
      $stmt->bindValue(':body', trim($this->body), PDO::PARAM_INT);
      $stmt->bindValue(':eyes', trim($this->eyes), PDO::PARAM_INT);

          
      // execute the query, also check if query was successful
      if($stmt->execute()){
        
        return true;
      }
      return false;
    }catch(Exception $ex){
      // set response code
      http_response_code(400);
      // display message: unable to create user
      echo json_encode(array("message" => $ex->getMessage(), "status" => "error", "code"=>$ex->getCode()));
    }
 }

}



