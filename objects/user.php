<?php
// 'user' object
class User {
  // database connection and table name
  private $conn;
  private $table_name = "usuario";
  // object properties
  public $id;
  public $userType;
  public $email;
  public $password;
  public $firstname;
  public $lastname;
  public $phone;
  public $profilePic;
  public $lat;
  public $lng;
  public $fb;
  public $ig;
  public $createdAt;
  public $updatedAt;

  //model
  public $heigth;
  public $hair;
  public $skin;
  public $body;
  public $eyes;

  //photographer
  public $camera;

  // constructor
  public function __construct($db) {
    $this->conn = $db;
  }
  // create new user record    
  function create() {
    try {
      // insert query
      $query = "INSERT INTO " . $this->table_name . "
                (
                  tipo_usuario,
                  email,
                  password,
                  nombre,
                  apellidos,
                  celular,
                  created_at,
                  updated_at
                )
                VALUES(
                  :userType,
                  :email,
                  :password,
                  :firstname,
                  :lastName,
                  :phone,
                  now(),
                  now()
                )";

      // prepare the query
      $stmt = $this->conn->prepare($query);
      // hash the password before saving to database
      $password_hash = password_hash($this->password, PASSWORD_BCRYPT);
      // bind the values
      $stmt->bindValue(':userType', trim($this->userType), PDO::PARAM_INT);
      $stmt->bindValue(':email', trim($this->email), PDO::PARAM_STR);
      $stmt->bindValue(':firstname', trim($this->firstname), PDO::PARAM_STR);
      $stmt->bindValue(':lastName', trim($this->lastname), PDO::PARAM_STR);
      $stmt->bindValue(':phone', trim($this->phone), PDO::PARAM_STR);
      $stmt->bindValue(':password', $password_hash, PDO::PARAM_STR);
      // execute the query, also check if query was successful
      if($stmt->execute()){
        return true;
      }
      return false;
    }catch(Exception $ex){
      // set response code
      http_response_code(400);
      // display message: unable to create user
      echo json_encode(array("message" => $ex->getMessage(), "status" => "error", "code"=>$ex->getCode()));
    }
  }

   // create new user record    
   function update() {
     // if atts need to be updated
    $password_set=!empty($this->password) ? "password = :password" : "";

    try {
      // insert query
      $query = "UPDATE " . $this->table_name . "
                SET
                  email = :email,
                  {$password_set},
                  nombre = :firstname,
                  apellidos = :lastName,
                  celular = :phone,
                  updated_at = NOW()
                WHERE id = :userId;";

      // prepare the query

      $stmt = $this->conn->prepare($query);
      // hash the password before saving to database
      $password_hash = password_hash($this->password, PASSWORD_BCRYPT);
      // bind the values
      $stmt->bindValue(':userId', trim($this->id), PDO::PARAM_INT);
      $stmt->bindValue(':email', trim($this->email), PDO::PARAM_STR);
      $stmt->bindValue(':firstname', trim($this->firstname), PDO::PARAM_STR);
      $stmt->bindValue(':lastName', trim($this->lastname), PDO::PARAM_STR);
      $stmt->bindValue(':phone', trim($this->phone), PDO::PARAM_STR);

      // hash the password before saving to database
      if(!empty($this->password)){
        $this->password=htmlspecialchars(strip_tags($this->password));
        $password_hash = password_hash($this->password, PASSWORD_BCRYPT);
        $stmt->bindValue(':password', $password_hash, PDO::PARAM_STR);
      }

      
      // execute the query, also check if query was successful
      if($stmt->execute()){
        return true;
      }
      return false;
    }catch(Exception $ex){
      // set response code
      http_response_code(400);
      // display message: unable to create user
      echo json_encode(array("message" => $ex->getMessage(), "status" => "error", "code"=>$ex->getCode()));
    }
  }


  
  // emailExists() method will be here
  function emailExist(){
    try{

      //validate if email exist query
      $query = "SELECT id, tipo_usuario, email, password,nombre, apellidos, created_at, updated_at
                FROM ".$this->table_name." 
                WHERE email = :email
                LIMIT 0,1;";
                 
      // prepare the query
      $stmt = $this->conn->prepare($query);
      // sanitize
      $this->email=htmlspecialchars(strip_tags($this->email));
      //bind the value
      $stmt->bindValue(':email', trim($this->email), PDO::PARAM_STR);
      //Execute query
      $stmt->execute();
      //Get rows number
      $num = $stmt->rowCount();
      
      //If email exist, asign values to object
      if($num > 0){
        //GET record details (values)
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        //Asign values to object
        $this->id = $row['id'];
        $this->email = $row['email'];
        $this->firstname = $row['nombre'];
        $this->lastname = $row['apellidos'];
        $this->phone = $row['celular'];
        $this->profilePic = $row['foto_perfil'];
        $this->height = $row['estatura'];
        $this->hair = $row['color'];
        $this->skin = $row['tez'];
        $this->body = $row['complexion'];
        $this->eyes = $row['ojos'];


        

        return true;
      }
      return false;

    }catch(Exception $ex){
      // set response code
      http_response_code(400);
      // display message: unable to create user
      echo json_encode(array("message" => $ex->getMessage(), "status" => "error", "code"=>$ex->getCode()));
    }
  }

  function getModel() {
    
    try {
      // insert query
      $query = "SELECT * FROM view_models
                WHERE id = :userId;";
  
      // prepare the query
    
      $stmt = $this->conn->prepare($query);
      // bind the values
      $stmt->bindValue(':userId', trim($this->userId), PDO::PARAM_INT);

  
          
       //Execute query
       $stmt->execute();
       //Get rows number
       $num = $stmt->rowCount();
       //If email exist, asign values to object
       if($num > 0){
         //GET record details (values)
         $row = $stmt->fetch(PDO::FETCH_ASSOC);
         //Asign values to object
         $this->id = $row['id'];
         $this->email = $row['email'];
         $this->password = $row['password'];
         $this->firstname = $row['nombre'];
         $this->lastname = $row['apellidos'];
         $this->phone = $row['celular'];
         $this->profilePic = $row['foto_perfil'];
         $this->height = $row['estatura'];
         $this->hair = $row['color'];
         $this->skin = $row['tez'];
         $this->body = $row['complexion'];
         $this->eyes = $row['ojos'];

        

        
        return true;
      }
      return false;
    }catch(Exception $ex){
      // set response code
      http_response_code(400);
      // display message: unable to create user
      echo json_encode(array("message" => $ex->getMessage(), "status" => "error", "code"=>$ex->getCode()));
    }
  }
  
  function getModels() {
    
    try {
      // insert query
      $query = "SELECT * FROM view_models;";
  
      // prepare the query
    
      $stmt = $this->conn->prepare($query);
         
       //Execute query
       $stmt->execute();
       //Get rows number
       return $stmt;
    }catch(Exception $ex){
      // set response code
      http_response_code(400);
      // display message: unable to create user
      echo json_encode(array("message" => $ex->getMessage(), "status" => "error", "code"=>$ex->getCode()));
    }
  }

  function getPhotographer() {
    
    try {
      // insert query
      $query = "SELECT * FROM view_photographers
                WHERE id = :userId;";
  
      // prepare the query
    
      $stmt = $this->conn->prepare($query);
      // bind the values
      $stmt->bindValue(':userId', trim($this->userId), PDO::PARAM_INT);

  
          
       //Execute query
       $stmt->execute();
       //Get rows number
       $num = $stmt->rowCount();
       //If email exist, asign values to object
       if($num > 0){
        //GET record details (values)
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        //Asign values to object
        $this->id = $row['id'];
        $this->email = $row['email'];
        $this->password = $row['password'];
        $this->firstname = $row['nombre'];
        $this->lastname = $row['apellidos'];
        $this->phone = $row['celular'];
        $this->profilePic = $row['foto_perfil'];
        $this->camera = $row['equipo'];
        
        return true;
      }
      return false;
    }catch(Exception $ex){
      // set response code
      http_response_code(400);
      // display message: unable to create user
      echo json_encode(array("message" => $ex->getMessage(), "status" => "error", "code"=>$ex->getCode()));
    }
  }
  
  function getPhotographers() {
    
    try {
      // insert query
      $query = "SELECT * FROM view_photographers;";
  
      // prepare the query
    
      $stmt = $this->conn->prepare($query);
         
       //Execute query
       $stmt->execute();
       //Get rows number
       return $stmt;
    }catch(Exception $ex){
      // set response code
      http_response_code(400);
      // display message: unable to create user
      echo json_encode(array("message" => $ex->getMessage(), "status" => "error", "code"=>$ex->getCode()));
    }
  }
}



