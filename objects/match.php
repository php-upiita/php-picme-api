<?php
// 'user' object
class Match {
  // database connection and table name
  private $conn;
  private $table_name = "picme.match";
  // object properties
  public $id_modelo;
  public $id_fotografo;
  public $status;
  public $created_at;
  public $userType;
  public $id;


  // constructor
  public function __construct($db) {
    $this->conn = $db;
  }
   // create new user record    
   function create() {
    
    try {
     // insert query
     $query = "INSERT INTO " . $this->table_name . "
                (
                  id_modelo,
                  id_fotografo,
                  status,
                  created_at
                )
                VALUES(
                  12,
                  11,
                  0,
                  NOW()
                )";

                  
     // prepare the query
   
     $stmt = $this->conn->prepare($query);
     // bind the values
     $stmt->bindValue(':id_modelo', trim($this->id_modelo), PDO::PARAM_INT);
     $stmt->bindValue(':id_fotografo', trim($this->id_fotografo), PDO::PARAM_INT);

       
     // execute the query, also check if query was successful
     if($stmt->execute()){
     
      
       return true;
     }
     return false;
    }catch(Exception $ex){
      // set response code
      http_response_code(400);
      // display message: unable to create user
      echo json_encode(array("message" => $ex->getMessage(), "status" => "error", "code"=>$ex->getCode()));
    }
 }

 function update() {
  $whereColumn= $this->userType == 1 ? "id_modelo = :id_usuario" : "id_fotografo = :id_usuario";

    
    try {
      // insert query
      $query = "UPDATE " . $this->table_name . "
                SET
                  status = :status
                WHERE {$whereColumn}";

      // prepare the query
    
      $stmt = $this->conn->prepare($query);
      // bind the values
      $stmt->bindValue(':status', trim($this->status), PDO::PARAM_INT);
      $stmt->bindValue(':id_usuario', trim($this->id_fotografo), PDO::PARAM_INT);

        
      // execute the query, also check if query was successful
      if($stmt->execute()){
      
        return true;
      }
      return false;
    }catch(Exception $ex){
      // set response code
      http_response_code(400);
      // display message: unable to create user
      echo json_encode(array("message" => $ex->getMessage(), "status" => "error", "code"=>$ex->getCode()));
    }
}

function getMatches() {
  $whoIsIt=$this->userType == 1 ? "usuario.id = picme.match.id_modelo" : "usuario.id = picme.match.id_fotografo" ;
  $whoIsIt2=$this->userType == 1 ? "usuario.id = :userId" : "usuario.id = :userId" ;
  try {
    // insert query
    $query = "SELECT usuario.id, usuario.email, usuario.nombre, usuario.apellidos, usuario.celular, usuario.foto_perfil, status 
               FROM ".$this->table_name." 
                INNER JOIN 
                      usuario on {$whoIsIt}
                      WHERE {$whoIsIt2} AND status = 1;";

    // prepare the query
    
    $stmt = $this->conn->prepare($query);
    $stmt->bindValue(':userId', trim($this->id), PDO::PARAM_INT);

     //Execute query
     $stmt->execute();
     //Get rows number
     return $stmt;
  }catch(Exception $ex){
    // set response code
    http_response_code(400);
    // display message: unable to create user
    echo json_encode(array("message" => $ex->getMessage(), "status" => "error", "code"=>$ex->getCode()));
  }
}

function getRequest() {
  $whoIsIt=$this->userType == 1 ? "usuario.id = picme.match.id_modelo" : "usuario.id = picme.match.id_fotografo" ;
  $whoIsIt2=$this->userType == 1 ? "usuario.id = :userId" : "usuario.id = :userId" ;
  try {
    // insert query
    $query = "SELECT usuario.id, usuario.email, usuario.nombre, usuario.apellidos, usuario.celular, usuario.foto_perfil, status 
               FROM ".$this->table_name." 
                INNER JOIN 
                      usuario on {$whoIsIt}
                      WHERE {$whoIsIt2} AND status = 0;";

    // prepare the query
    
    $stmt = $this->conn->prepare($query);
    $stmt->bindValue(':userId', trim($this->id), PDO::PARAM_INT);

     //Execute query
     $stmt->execute();
     //Get rows number
     return $stmt;
  }catch(Exception $ex){
    // set response code
    http_response_code(400);
    // display message: unable to create user
    echo json_encode(array("message" => $ex->getMessage(), "status" => "error", "code"=>$ex->getCode()));
  }
}

}



