<?php
// 'user' object
class Photographer {
  // database connection and table name
  private $conn;
  private $table_name = "fotografo";
  // object properties
  public $userId;
  public $camera;

  // constructor
  public function __construct($db) {
    $this->conn = $db;
  }
   // create new user record    
   function update() {
    
   try {
     // insert query
     $query = "UPDATE " . $this->table_name . "
               SET
                 equipo = :camera
               WHERE id_usuario = :userId;";

     // prepare the query
   
     $stmt = $this->conn->prepare($query);
     // bind the values
     $stmt->bindValue(':userId', trim($this->userId), PDO::PARAM_INT);
     $stmt->bindValue(':camera', trim($this->camera), PDO::PARAM_STR);
       
     // execute the query, also check if query was successful
     if($stmt->execute()){
     
      
       return true;
     }
     return false;
   }catch(Exception $ex){
     // set response code
     http_response_code(400);
     // display message: unable to create user
     echo json_encode(array("message" => $ex->getMessage(), "status" => "error", "code"=>$ex->getCode()));
   }
 }

  

}



